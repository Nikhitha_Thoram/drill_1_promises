const fs = require("fs").promises;
const path = require("path");

function createAndDeleteFiles(dirName) {
  let randomNumber = Math.round(Math.random() * 10);
  let filesCreated = 0;

  return fs
    .mkdir(dirName)
    .then(() => {
      console.log("directory created successfully");
      for (let index = 1; index <= randomNumber; index++) {
        let filePath = path.join(dirName, `file_${index}.json`);
        let fileContent = JSON.stringify({ a: 10, b: 20 });
        fs.writeFile(filePath, fileContent)
          .then(() => {
            filesCreated++;
            console.log(`file_${index}.json created successfully`);
            if (filesCreated === randomNumber) {
              deleteFiles(dirName, randomNumber);
            }
          })
          .catch((error) => {
            console.error(`error creating a file_${index}.json`, error);
          });
      }
    })
    .catch((error) => {
      console.error("error creating directory", error);
    });

  function deleteFiles(dirName, numberOfFiles) {
    for (let index = 1; index <= numberOfFiles; index++) {
      let filePath = path.join(dirName, `file_${index}.json`);
      fs.unlink(filePath)
        .then(() => {
          console.log(`file_${index}.json deleted successfully`);
        })
        .catch((error) => {
          console.error(`error deleting file_${index}.json`, error);
        });
    }
  }
}

module.exports = createAndDeleteFiles;
