const fs = require("fs").promises;

function fileOperations(fileName) {
  return fs.readFile(fileName, "utf-8")
    .then((data) => {
      return fs.writeFile("upperCaseFile.txt", data.toUpperCase()).then(() => {
        console.log(
          "data converted to uppercase and written to file upperCaseFile.txt"
        );
        return fs.appendFile("filenames.txt", "upperCaseFile.txt\n");
      });
    })
    .then(() => {
      console.log("filename appended to filenames.txt");
      return fs.readFile("upperCaseFile.txt", "utf-8");
    })
    .then((upperCaseContent) => {
      const lowerCaseContent = upperCaseContent.toString().toLowerCase().split(".").join(".\n");
      return fs.writeFile("lowerCaseFile.txt", lowerCaseContent).then(() => {
        console.log(
          "data converted to lowerCase and written to file lowerCaseFile.txt"
        );
        return fs.appendFile("filenames.txt", "lowerCaseFile.txt\n");
      });
    })
    .then(() => {
      console.log("filename appended to the file filenames.txt");
      return Promise.all([
        fs.readFile("upperCaseFile.txt", "utf-8"),
        fs.readFile("lowerCaseFile.txt", "utf-8"),
      ]).then((files) => {
        const sortedContent = files.map((content) =>
          content.split("\n").sort()
        );
        return fs.writeFile("sortFile.txt", sortedContent.join("\n"))
          .then(() => {
            console.log("data sorted and written to file sortFile.txt");
            return fs.appendFile("filenames.txt", "sortFile.txt");
          });
      });
    })
    .then(() => {
      console.log("filename appended to filenames.txt");
      return fs.readFile("filenames.txt", "utf-8");
    })
     
    .then((data) => {
      const filesToDelete = data.trim().split("\n");
      return Promise.all(filesToDelete.map((file) => fs.unlink(file))).then(
        () => {
          console.log("all files deleted successfully");
        }
      );
    })

    .catch((error) => {
      console.error(error);
    });
}

module.exports = fileOperations;
